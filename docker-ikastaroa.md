# docker ikastaro praktikoa

GNU/Linux, sistema kudeaketan zein software garapenean ezagutza gomendagarria da: terminala erabiltzen jakitea, komando ezberdinen ezagutza, `git`... Dena dela, ahalik eta orokorrena izango da ikastaroa.

## (1. eguna) Hastapenak

* Azalpen laburra
* Irudia vs Edukiontzia
* Linux edukiontzi teknologiak

### hub.docker.com

Docker irudi erregistro nagusi/ofiziala.

### Martxan jartzen

Docker edukiontziak martxan jartzerakoan ditugun aukera nagusienak ikusiko ditugu:

* *volume*-ak
* portuak
* loturak
* sareak
*...

### docker-ekin lanean

Oinarriak ikasita docker edukiotziekin lan ezberdinak egingo ditugu:

* `exec`
* `logs`
* `commit`
* `inspect`
* ...

### Dockerfile

`Dockerfile`-a erabilitz Docker irudiak sortzen ikasiko dugu:

* `Dockerfile`-a
* `build`
* `push`
* ...


## (2. eguna) Orkestazioa eta DevOps

### docker-compose

`docker-compose` erabiltzen ikasiko dugu:

* `docker-compose.yml` fitxategia
* komando nagusiak

### Docker erregistro pribatua

Docker erregistro pribatu aukera ezberdinak

### Orkestatzaileak

Docker orkestatzaile batek ematen dizkigun aukerak ikusiko ditugu eta merkatuan dauden soluzio libre ezberdinak aztertuko ditugu:

* Kubernetes
* Swarm
* Rancher

### CI (Countinuous Integration)

GitLab erabiliko dugu Docker-en oinarritutako CI *pipeline*-ak sortzeko.

### CD (Countinuous Delivery)

GitLab eta Rancher erabiliko ditugu CI/CD, hau da, software eraikitze eta ezartze prozesuak automatizatzeko.
