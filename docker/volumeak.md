# Volume-ak

## Kudeaketa

### docker volume create

Sortu *volume* bat.

```
docker volume create vol-bat
```

### docker volume ls

Zerrendatu *volume*-ak.

```
docker volume ls
```

### docker volume inspect

*Volume*-a nun dagoen jakiteko interesgarria da.

```
docker volume inspect
```

### docker volume rm

Ezabatu *volume*-a.

```
docker volume create vol-bat
```

::: tip ARIKETA
Datubasearentzako *volume* bat sortu eta hau erabiliko dugu datubaseak datuak gorde ditzan.

**PROTIP**: existitzen ez den *volume* baten izena pasa eta automatikoki sortuko du.
:::

::: tip ARIKETA
`docker inspect` erabili *volume*-a ondo muntatu dela ikusteko.
:::

::: tip ARIKETA
`docker volume ls` erabili *volume*-a zerrendan ikusteko.
:::

## Volume-a bete edukiontzi bat erabilita

[docs](https://docs.docker.com/storage/volumes/#populate-a-volume-using-a-container)

*Volume* bat sortzen duen edukiontzia sortzean, edukiontzian muntatutako direktorioan fitxategiak badaude, hauek *volume*-ra kopiatu eta edukiontzi (zein besteek) erabili ahal izango dituzte.

## Irakurketa moduan bakarrik

*Volume*-ak (`bind mount`-ak baita ere) irakurtzeko moduan muntatu daitezke, edukiontziak idatzi ahal izango ez duelarik.

`docker run ... -v /zerbitzarian/bide/bat:/edukiontzian/beste/bat:ro ...`
`docker run ... -v volume-bat:/edukiontzian/bide/bat:ro ...`


## Volume driver-ak

TODO

## Backup, restore, migrate

TODO

https://docs.docker.com/storage/volumes/#backup-restore-or-migrate-data-volumes
